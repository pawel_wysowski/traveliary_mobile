const strings = {
    home: {
        buttons: {
        },
        inputs: {
        },
        labels: {
            travels: 'Travels',
            favoritePlaces: 'Favorite places',
        },
    }
}

export default strings;