import strings from './strings';
import colors from './colors';
import fonts from './fonts';

const R = {
    strings,
    colors,
    fonts,
}

export default R;