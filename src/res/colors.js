const colors = {
    primary: '#770111',
    secondary: '#ED6E78',
    complementary: '#046290',
    overlay: '#b39cd0',
    background: '#53D0EC',
    card: '#F5F5F5',
}

export default colors;