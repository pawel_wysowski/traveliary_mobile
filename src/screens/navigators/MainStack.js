import MainScreen from '../main_screen/MainScreen'

const MainStackNavigator = {
        Main: {
          screen: MainScreen,
        },
}

export default MainStackNavigator;