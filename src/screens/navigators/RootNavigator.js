import MainStack from './MainStack';
import LoginStack from './LoginStack';
import { createStackNavigator, createAppContainer, createSwitchNavigator } from "react-navigation";
import R from '../../res/R';

const config = {
    defaultNavigationOptions: {
        headerTransparent: true
    }
};

const HomeContainer = createStackNavigator(MainStack, config);
const LoginContainer = createStackNavigator(LoginStack, config);

const RootNavigatorRoutes = {
    LoginStack: {
        screen: LoginContainer,
    },
    HomeStack: {
        screen: HomeContainer,
    },
};

const navigatorConfig = {
	headerMode: 'none',
	initialRouteName: 'LoginStack',
};


const RootNavigator = createSwitchNavigator(RootNavigatorRoutes, navigatorConfig);
const RootContainer = createAppContainer(RootNavigator);
export default RootContainer;