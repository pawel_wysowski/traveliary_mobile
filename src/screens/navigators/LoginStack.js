import LoginScreen from '../login_screen/LoginScreen';
import SignupScreen from '../signup_screen/SignupScreen';

const LoginStackNavigator = {
        Login: {
          screen: LoginScreen,
        },
        Signup: {
          screen: SignupScreen,
        }
}

export default LoginStackNavigator;