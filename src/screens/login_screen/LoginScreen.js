import React, { Component } from 'react'
import { Text, View, Image, TextInput, Animated, Keyboard, Dimensions } from 'react-native'
import * as firebase from 'firebase';
import config from '../../config';
import styles from './LoginScreen.style';
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { logo, plane_l, woman_r } from '../../res/images';
import R from '../../res/R';
import * as Font from 'expo-font';

export default class LoginScreen extends Component {

    constructor(props){
        super(props);
        this.animatedValue = new Animated.Value(0);
        this.navigator = props.navigation;
        this.state = {
            email: '',
            password: '',
            y: new Animated.Value(-300),
            flex: new Animated.Value(1),
            fontLoaded: false,
        };
        firebase.initializeApp(config);
    }
  
    async componentDidMount() {
        this.move();
        await Font.loadAsync({
            'raleway-regular': require('fonts/raleway_regular.ttf'),
        });
        this.setState({fontLoaded: true});
    }


    animateFlex = () => {
        Animated.spring(
            this.state.flex, {
                toValue: .5,
            }).start();
    };

    animateFlexBack = () => {
        Animated.spring(
            this.state.flex, {
                toValue: .75,
            }).start();
    };

    moveUp = () => {
        Animated.spring(this.state.y, {
          toValue: 300,
        }).start();
      };

    move = () => {
        Animated.spring(this.state.y, {
          toValue: -10,
        }).start();
      };

    signIn = () => {
        // const {email, password} = this.state;
        const  email = 'wysowski.pawel@gmail.com';
        const password = 'Wysowski96';
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if(regex.test(email) === false){
            console.log('Error logging');
        }
        else{
            try {
                firebase.auth().signInWithEmailAndPassword(email, password);
                firebase.auth().onAuthStateChanged(user => {
                    if(user)
                    this.props.navigation.navigate('HomeStack');
                })
          } catch (error) {
                console.log('Error');
          }
        }
    };

    signUp = () => {
        this.navigator.navigate('Signup');
    };

    handleAnimation = () => {
        // A loop is needed for continuous animation
        Animated.loop(
          // Animation consists of a sequence of steps
          Animated.sequence([
            // start rotation in one direction (only half the time is needed)
            Animated.timing(this.animatedValue, {toValue: 1.0, duration: 150, easing: Easing.linear, useNativeDriver: true}),
            // rotate in other direction, to minimum value (= twice the duration of above)
            Animated.timing(this.animatedValue, {toValue: -1.0, duration: 300, easing: Easing.linear, useNativeDriver: true}),
            // return to begin position
            Animated.timing(this.animatedValue, {toValue: 0.0, duration: 150, easing: Easing.linear, useNativeDriver: true})
          ])
        ).start(); 
    }

    onPasswordChange = pwd =>{
        this.setState({password: pwd});
    };

    onEmailChange = email =>{
        this.setState({email});
    };

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
     }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
      _keyboardDidShow = () => {
          this.animateFlex();
      }
    
      _keyboardDidHide = () => {
        this.animateFlexBack();
      }

    render() {
        const {email, password, y, flex, fontLoaded} = this.state;
        return (
            <View style={styles.mainContainer} onPress={Keyboard.dismiss}>
                    <Image source={plane_l} style={{position: 'absolute', top: 0, left: 0, maxHeight: '50%', maxWidth: '50%'}} resizeMode="contain" />
                    <Image source={woman_r} style={{position: 'absolute', bottom: 0, left: '70%', height: '30%', width: '30%'}} resizeMode="contain" />
                    <Animated.View style={[styles.backgroundContainer, { flex: flex}]}>
                       <Image source={logo} resizeMode="contain"/>
                    </Animated.View>
                    <Animated.View style={[styles.loginCard, {bottom: y}]}>
                    <View style={styles.loginForm}>
                      {fontLoaded && <Text style={[styles.inputLabel, {fontFamily: 'raleway-regular'}]}>Email</Text>}
                        <TextInput value={email} onChangeText={this.onEmailChange} style={styles.input}/>
                        
                        {fontLoaded && <Text style={[styles.inputLabel, {fontFamily: 'raleway-regular'}]}>Password</Text>}
                        <TextInput secureTextEntry value={password} onChangeText={this.onPasswordChange} style={styles.input} />
                    </View>
                    <View style={styles.buttonContainer}>
                    <TouchableOpacity onPress={this.signIn} style={styles.loginButton}>
                        {fontLoaded &&<Text style={[styles.loginButtonText, {fontFamily: 'raleway-regular'}]}>Login</Text>}
                    </TouchableOpacity> 
                    <TouchableOpacity onPress={this.signUp}>
                        {fontLoaded && <Text style={{fontFamily: 'raleway-regular', color: R.colors.card, marginTop: 5}}>Don't have an account? Sign up</Text>}  
                    </TouchableOpacity> 
                    </View>
                </Animated.View>
            </View>
            
        )
    }
}
