import R from '../../res/R';
import {StyleSheet, Dimensions} from 'react-native';

const fullWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: R.colors.background,
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    card: {
        backgroundColor: R.colors.card,
        elevation: 8,
        shadowOpacity: 10,
        borderRadius: 5,
        flex: 1,
        maxHeight: 200,
        minWidth: 300,
        justifyContent: 'flex-start',
        alignItems: 'center',
        margin: 30,
    },
    cardImage: {
        flex: 4,
        width: '100%',
    },
    cardHeader: {
        flex: 1,
        width: '100%',
        marginHorizontal: 15,
        padding: 10,
        paddingTop: 0,
        paddingBottom: 20,
        paddingLeft: 20,
        justifyContent: 'flex-end',
    },
    cardHeaderText: {
        color: R.colors.complementary,
        fontWeight: 'bold',
        fontSize: 18,
    },
    cardImageFill: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'cover',
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
    },
    travelsContainer: {
        flex: 1.2,
        alignItems: 'center',
        backgroundColor: R.colors.card,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
    },
    favoritePlacesContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        backgroundColor: R.colors.secondary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        marginTop: -60,
        elevation: 8,
        zIndex: 2,
    },
    travelsText: {
        color: R.colors.complementary,
        fontWeight: 'bold',
        fontFamily: 'sans-serif-light',
        fontSize: 25,
        marginTop: 25,
    },
    placesText: {
        color: R.colors.card,
        fontWeight: 'bold',
        fontFamily: 'sans-serif-light',
        fontSize: 25,
        marginTop: 25,
    },
    upperImage: {
        position: 'absolute',
        top: 0,
        flex: 1,
        width: '100%',
        height: 250,
        resizeMode: 'cover',
    },
    lowerContentContainer: {
        flex: 3,
        width: '100%',
        backgroundColor: R.colors.card,
        borderTopRightRadius: 60,
        borderTopLeftRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 200,
        elevation: 8,
        zIndex: 2,
    },
    flatListContainer: {
        marginTop: 20,
        paddingHorizontal: 30,
        justifyContent: 'center',
        maxHeight: 280,
    },
    flatListParent: {
        flex: 1,
    }
});

export default styles;