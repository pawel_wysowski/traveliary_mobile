import React, { Component } from 'react'
import { Text, View, Image, StatusBar } from 'react-native'
import styles from './MainScreen.style';
import { mainUpperPlanes } from '../../res/images';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import R from '../../res/R';
import firebase from 'firebase';

const travels = [
    {id: 0, name: "Portugal"}
];

export default class MainScreen extends Component {

    constructor(props){
        super(props);
        
        this.user = firebase.auth().currentUser;
    }

    getUserData(snapshot) {
        
    }

    componentDidMount() {

        let travelss = [];
        if(this.user!==null)
            firebase.database().ref('users').once('value').then(snapshot => {
            // this.userData = this.getUserData(snapshot, user);

            // firebase.database().ref('travels').once('value').then( tSnapshot => {
            //    this.userData.travels.forEach(element => {
            //         travelss = tSnapshot.filter( t => t.uid === element.uid);
            //    });
            // })
            snapshot.forEach( element => {
            
            if(element.val().uid == this.user.uid)
               firebase.database().ref('travels').once('value').then( t => {
            element.val().travels.forEach( x => {
                console.log(t.name())
                if( x.uid == t.name())
                    travelss.push(t.val());
            })
        }
               )
            })
         });
         console.log(travelss.length);
    }

    renderCard(item){
        return(
            <TouchableOpacity style={styles.card} activeOpacity={0.7}>
                <View style={styles.cardImage}>
                    <Image source={ mainUpperPlanes } style={styles.cardImageFill}/>
                </View>
                <View style={styles.cardHeader}>
                    <Text style={styles.cardHeaderText}> {item.name} </Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Image source={ mainUpperPlanes } style={styles.upperImage}/>
                <View style={styles.lowerContentContainer}>
                    <View style={styles.travelsContainer}>
                        <Text style={styles.travelsText}>{R.strings.home.labels.travels}</Text>
                        <View style={styles.flatListParent}>
                            <FlatList
                            contentContainerStyle={styles.flatListContainer}
                            data={travels}
                            renderItem={({item}) => this.renderCard(item)}
                            horizontal
                            />
                        </View>
                    </View>
                    <View style={styles.favoritePlacesContainer}>
                        <Text style={styles.placesText}>{R.strings.home.labels.favoritePlaces}</Text>
                        <View style={styles.flatListParent}>
                            <FlatList
                            contentContainerStyle={styles.flatListContainer}
                            data={travels}
                            renderItem={({item}) => this.renderCard(item)}
                            horizontal
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
