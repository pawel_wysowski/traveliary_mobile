import React, { Component } from 'react'
import { Text, View, Image, TextInput, Animated, Keyboard, Dimensions } from 'react-native'
import * as firebase from 'firebase';
import styles from './SignupScreen.style';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { logo, signup_l, signup_r } from '../../res/images';
import R from '../../res/R';
import * as Font from 'expo-font';

export default class SignupScreen extends Component {

    constructor(props){
        super(props);
        this.animatedValue = new Animated.Value(0);
        this.navigator = props.navigation;
        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            y: new Animated.Value(-300),
            flex: new Animated.Value(1),
            fontLoaded: false,
        };
    }
  
    async componentDidMount() {
        this.move();
        await Font.loadAsync({
            'raleway-regular': require('fonts/raleway_regular.ttf'),
        });
        this.setState({fontLoaded: true});
    }


    animateFlex = () => {
        Animated.spring(
            this.state.flex, {
                toValue: .5,
            }).start();
    };

    animateFlexBack = () => {
        Animated.spring(
            this.state.flex, {
                toValue: .75,
            }).start();
    };

    moveUp = () => {
        Animated.spring(this.state.y, {
          toValue: 300,
        }).start();
      };

    move = () => {
        Animated.spring(this.state.y, {
          toValue: -10,
        }).start();
      };

    signUp = () => {
        const {email, password, firstName, lastName} = this.state;
        
        const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const regexPass = /^(?=.\d)(?=.[a-z])(?=.*[A-Z]).{6,20}$/;

        if(regexMail.test(email) === false || regexPass.test(password) || firstName.length == 0 || lastName.length == 0){
            console.log('Error logging');
        }
        else{
            try {
                firebase.auth().createUserWithEmailAndPassword(email, password).then( 
                    data => { 
                        this.props.navigation.navigate('HomeStack');
                        firebase.database().ref().child('users')
                        .push({
                            uid: data.user.uid,
                            name: firstName,
                            surname: lastName,
                            travels: [],
                        }); 
                })
          } catch (error) {
                console.log(error);
          }
        }
    };

    onPasswordChange = pwd =>{
        this.setState({password: pwd});
    };

    onEmailChange = email =>{
        this.setState({email});
    };

    onFirstNameChange = firstName =>{
        this.setState({firstName});
    };

    onLastNameChange = lastName =>{
        this.setState({lastName});
    };

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
     }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
      _keyboardDidShow = () => {
          this.animateFlex();
      }
    
      _keyboardDidHide = () => {
        this.animateFlexBack();
      }

    render() {
        const {email, password, firstName, lastName, y, flex, fontLoaded} = this.state;
        return (
            <View style={styles.mainContainer} onPress={Keyboard.dismiss}>
                    <Image source={signup_l} style={{position: 'absolute', bottom: 0, left: 0, maxHeight: '50%', maxWidth: '50%'}} resizeMode="contain" />
                    <Image source={signup_r} style={{position: 'absolute', top: 0, left: '70%', height: '30%', width: '30%'}} resizeMode="contain" />
                    <Animated.View style={[styles.backgroundContainer, { flex: flex}]}>
                       <Image source={logo} resizeMode="contain"/>
                    </Animated.View>
                    <Animated.View style={[styles.loginCard, {bottom: y}]}>
                    <View style={styles.loginForm}>

                        <View style={styles.loginFormCredentials}>
                            <View style={{flex: 1, width: '40%'}}>
                                {fontLoaded && <Text style={[styles.inputLabel, {fontFamily: 'raleway-regular', flex: 1}]}>First name</Text>}
                                <TextInput value={firstName} onChangeText={this.onFirstNameChange} style={[ styles.inputOverride]}/>
                            </View>
                            <View style={{flex: 1, width: '40%'}}>
                                {fontLoaded && <Text style={[styles.inputLabel, {fontFamily: 'raleway-regular', flex: 1}]}>Last name</Text>}
                                <TextInput value={lastName} onChangeText={this.onLastNameChange}  style={[styles.inputOverride]} />
                            </View>
                        </View>
                        <View style={{flex: 3}}>
                            {fontLoaded && <Text style={[styles.inputLabel, {fontFamily: 'raleway-regular'}]}>Email</Text>}
                            <TextInput value={email} onChangeText={this.onEmailChange} style={styles.input}/>
                            
                            {fontLoaded && <Text style={[styles.inputLabel, {fontFamily: 'raleway-regular'}]}>Password</Text>}
                            <TextInput secureTextEntry value={password} onChangeText={this.onPasswordChange} style={styles.input} />
                        </View>
                    </View>
                    <View style={{alignItems: 'center', marginHorizontal: 20}}>
                        {fontLoaded && <Text style={{fontFamily: 'raleway-regular', textAlign: 'center', color: R.colors.card, marginTop: 5}}>By clicking Sign Up, you agree to Privacy Policy and Cookie Policy</Text>}  
                    </View> 
                    <View style={styles.buttonContainer}>
                    <TouchableOpacity onPress={this.signUp} style={styles.loginButton}>
                        <Text style={[styles.loginButtonText, {fontFamily: 'raleway-regular'}]}>Sign Up</Text>
                    </TouchableOpacity>
                    </View>
                </Animated.View>
            </View>
            
        )
    }
}
