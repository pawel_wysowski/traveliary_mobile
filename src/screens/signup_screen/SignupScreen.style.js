import R from '../../res/R';
import {StyleSheet, Dimensions} from 'react-native';
const fullWidth = Dimensions.get('window').width;
const fullHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: R.colors.background,
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundImage: {
        position: 'absolute',
    },
    backgroundContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 200,
    },
    backgroundImage: {
        flex: 1,
        backgroundColor: R.colors.background,
    },
    loginCard: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 20,
        marginBottom: 30,
    },
    cardHeader: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderText: {
        color: R.colors.complementary,
        fontWeight: 'bold',
        fontSize: 18,
    },
    loginForm: {
        flex: 2.5,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 20,
    },
    input: {
        paddingHorizontal: 15,
        marginHorizontal: 15,
        marginTop: 10,
        minWidth: '100%',
        height: 35,
        borderColor: R.colors.complementary,
        borderWidth: 1,
        borderRadius: 33,
        backgroundColor: R.colors.card,
        fontSize: 13,
    },
    inputOverride: {
        flex: 1,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        borderColor: R.colors.complementary,
        borderWidth: 1,
        borderRadius: 33,
        backgroundColor: R.colors.card,
        fontSize: 13,
    },
    inputLabel: {
        color: R.colors.card,
        alignSelf: 'flex-start',
        marginLeft: 35,
        marginTop: 10,
    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginButton: {
        backgroundColor: R.colors.complementary,
        minWidth: 270,
        height: 40,
        borderRadius: 33,
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginButtonText: {
        color: R.colors.card,
        fontSize: 15,
    },
    loginFormCredentials: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    }
});

export default styles;