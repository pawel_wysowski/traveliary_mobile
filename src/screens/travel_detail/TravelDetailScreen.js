import React, { Component } from 'react'
import { Text, View, Image, StatusBar } from 'react-native'
import styles from './MainScreen.style';
import { mainUpperPlanes } from '../../res/images';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import R from '../../res/R';

const travels = [
    {id: 0, name: "Portugal"},
    {id: 1, name: "Spain"},
    {id: 2, name: "USA"},
    {id: 3, name: "Canada"},
];

export default class MainScreen extends Component {

    renderCard(item){
        return(
            <TouchableOpacity style={styles.card} activeOpacity={0.7}>
                <View style={styles.cardImage}>
                    <Image source={ mainUpperPlanes } style={styles.cardImageFill}/>
                </View>
                <View style={styles.cardHeader}>
                    <Text style={styles.cardHeaderText}> {item.name} </Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar translucent backgroundColor="rgba(0,0,0,0.2)"/>
                <Image source={ mainUpperPlanes } style={styles.upperImage}/>
                <View style={styles.lowerContentContainer}>
                    <View style={styles.travelsContainer}>
                        <Text style={styles.travelsText}>{R.strings.home.labels.travels}</Text>
                        <View style={styles.flatListParent}>
                            <FlatList
                            contentContainerStyle={styles.flatListContainer}
                            data={travels}
                            renderItem={({item}) => this.renderCard(item)}
                            horizontal
                            />
                        </View>
                    </View>
                    <View style={styles.favoritePlacesContainer}>
                        <Text style={styles.placesText}>{R.strings.home.labels.favoritePlaces}</Text>
                        <View style={styles.flatListParent}>
                            <FlatList
                            contentContainerStyle={styles.flatListContainer}
                            data={travels}
                            renderItem={({item}) => this.renderCard(item)}
                            horizontal
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
