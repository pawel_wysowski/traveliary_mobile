import React from 'react';
import RootNavigator from './src/screens/navigators/RootNavigator';

export default function App() {
  return (
    <RootNavigator/>
  );
}
